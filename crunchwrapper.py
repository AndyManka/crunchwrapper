'''Build a hack of Golf2 that combines:
Golf 1
Golf 1 Lost Islands expansion
Golf 2
Golf 2 Fairy Tales expansion
Also patches out demo checks, does misc. hacks, and
attempts to mimic the full 360 release of golf 2. Nowhere near complete yet.'''

import shutil
import os
import logging
import coloredlogs
import pdb
import itertools
import filecmp
import re
from datetime import datetime
from binaryornot.check import is_binary
from bs4 import UnicodeDammit

logging.getLogger().setLevel(logging.DEBUG)
coloredlogs.install(level="debug", fmt='%(message)s')
logging.getLogger('chardet').setLevel(logging.WARNING)
logging.getLogger("binaryornot").setLevel(logging.WARNING)

RUNTIME = datetime.today().strftime('%Y-%m-%d-%H_%M_%S')

DO_GOLF1 = False
build_note = ''
if DO_GOLF1:
    logging.warning("Golf 1 is broken currently. you're on your own.")
    build_note = build_note + "_golf1"

# all other constant dirs are relative to PATH_ROOT
PATH_ROOT = "E:\\golf\\"
# where the 'compiled' game, with all hacks and patches, is built
PATH_DEST = os.path.join(PATH_ROOT, "builds\\", RUNTIME + build_note + "\\")
# without the trailing \\, os.path.join reads it as an "absolute path"
# and discards all previous inputs
# "clean" = unedited extracts of data.pak -like files made with QuickBMS.
PATH_GOLF1 = os.path.join(PATH_ROOT, 'cleanGolf1\\')
PATH_GOLF1_EX = os.path.join(PATH_ROOT, 'cleanGolf1-ex\\')
PATH_GOLF2 = os.path.join(PATH_ROOT, "cleanpc\\")
PATH_360 = os.path.join(PATH_ROOT, "clean360\\")
# 360 release of fairy DLC, extracted
PATH_GOLF2_EX = os.path.join(PATH_ROOT, "clean-fairy\\")


def add_content(golf1, golf1_ex, golf2, golf2_ex, x360, dest_dir):
    # concatenated files will be in this order: g1, g1ex, g2_pc, g2ex, g2_360
    # hopefully this minimizes conflicts with golf1 renaming golf2 functions.
    # assumption: game loads scripts like most programming languages:
    # identical conflicts are overwritten with the final (end of file)
    # version. but in testing, doesn't pan out - golf1 at the top of the file
    # seems to cause a golf1 load to occur.
    source_list_old_to_new = [golf1, golf1_ex, golf2, golf2_ex, x360]
    source_list_option_2 = [golf2, golf2_ex, x360, golf1, golf1_ex]
    # TODO undo source_list and see if it still works
    source_list = source_list_option_2
    if source_list != source_list_old_to_new:
        logging.warning("trying out some stuff")
    # source_list.reverse()
    # source_list = [golf1, golf1_ex, golf2, x360, golf2_ex]
    if not DO_GOLF1:
        logging.debug("Skip golf1")
        source_list.remove(golf1)
        source_list.remove(golf1_ex)
    do_360 = False
    if not do_360:
        source_list.remove(x360)
        logging.warning("no full 360 but yes copying maps")
    logging.debug("source_list is {}".format(source_list))
    for src in (source_list):
        logging.info("Now on {}".format(src))
        # newer sources are always overwriting or appending earlier files.
        # for example, golf1_ex overwrites golf1. golf2_ex overwrites golf2.
        #
        # initialize dir with golf1 or golf2 (base game, no expansions).
        # then copy in the extra files, with the tiebreaking in favor of `src`.
        merge_dirs(dest_dir, dest_dir, src)
    x360_maps = os.path.join(x360, 'data', 'maps')
    out_maps = os.path.join(dest_dir, 'data', 'maps')
    logging.info("Adding 360 maps")
    # todo add precache folder? or maybe just merge the whole 360 dir?
    merge_dirs(out_maps, x360_maps, out_maps)
    # get the templates to make fairy work

    tmpl_dest = os.path.join(dest_dir, 'data', 'templates')
    tmpl_golf1 = os.path.join(dest_dir, 'data', 'templates_pc')
    # templates_contentex01 is for BOTH golf1 expansion AND golf2 fairy
    tmpl_exp = os.path.join(dest_dir, 'data', 'templates_contentex01')
    # trying to do this after merging golf2_ex with golf2.
    # maybe functions are "set once then immutable"?
    # so you wouldn't want a golf1 function overwriting
    # something in golf2 by doing it golf1-then-golf2
    # todo what if we rename all golf1 functions and paths to "golf1_ogpath"?
    if DO_GOLF1:
        logging.warning("no golf1 tepmlates")
        # merge_dirs(tmpl_dest, tmpl_golf1, tmpl_dest)
    logging.debug("merging templ_exp with templ_dest")
    merge_dirs(tmpl_dest, tmpl_exp, tmpl_dest)


def copy_file(src_path, dest_path):
    '''Copy a file from src_path to dest_path. Returns `True` on success
    or `False` on failures. Uses `shutil.copy2`.'''
    if not (os.path.exists(src_path)):
        logging.warning("src doesn't exist")
        return False
    if os.path.exists(dest_path):
        if os.path.samefile(src_path, dest_path):
            logging.debug("Same file")
            return False
        # logging.warning("dest_path exists {}".format(dest_path))
    try:
        shutil.copy2(src_path, dest_path)
        return True
    except Exception as e:
        print(e)
        return False


def mod_guigroup(in_text, enable_debug=True):
    logging.info("crack the demo")
    out_text = in_text.replace("GetPurchaseStatus()",
                               '(true or GetPurchaseStatus())')
    if enable_debug:
        # only affects one code block?
        # ```if not(IsFinal()) then
        #   me.version.Text = gui.VersionString```
        logging.info('Set IsFinal() to opposite (false?)')
        out_text = out_text.replace("IsFinal()", '(not IsFinal())')
    skip_intros = True
    if skip_intros:
        # I avoided a more invasive intro skip. Ex. jumping right to the main menu.
        # Because I'm not sure what unintended side effects that may have.
        # The game may use these screens to initialize stuff in the background.
        # Ex. there's a bunch of stuff in gui/gui/splash/main
        logging.info('skip intros')
        wait_dict = {
            # Wanako
            'Wait(1)#r#ng': 'Wait(0)#r#ng',
            '1#")#r#nWait(1.5)': '1#")#r#nWait(0)',
            # Rating
            'Wait(0.1)#r#n#tme.text.Visible = 1#r#nend#r#n#r#nme.black_background.QueueBh2(#"B2FadeTo#",#"1,0,1#")#r#n#r#nWait(0.01)#r#nme.rating.Visible = 1#r#n#r#nWait(3.5)#r#nme.black_background.QueueBh2(#"B2FadeTo#",#"0,1,1#")#r#n#r#nWait(1)#r#nme.RunCmd(0)")': 'Wait(0)#r#n#tme.text.Visible = 1#r#nend#r#n#r#nme.black_background.QueueBh2(#"B2FadeTo#",#"1,0,1#")#r#n#r#nWait(0)#r#nme.rating.Visible = 1#r#n#r#nWait(0)#r#nme.black_background.QueueBh2(#"B2FadeTo#",#"0,1,1#")#r#n#r#nWait(0)#r#nme.RunCmd(0)")',
            # Konami
            'Wait(3)': 'Wait(0)',
            # note the space
            'Wait(1) ': 'Wait(0) '
        }
        for old, new in wait_dict.items():
            out_text = out_text.replace(old, new)
    return out_text


def mod_guilabel(in_text):
    # some kind of text lookup table. as well as initializing these strings.
    # text appears to be encoded / encrypted
    # longer-seeming messages have longer encodings. plain text
    # sometimes/always works in the encoded string's place.
    # achievements
    # WCbBA6AAnBA1BApBAvAAnBA1BApBAvAAtBAhBApBAuBAvAA0BAvBAvBAsBA0BApBAwBAzBAvAAhBAjBAoBApBAlBA2BAlBAtBAlBAuBA0BAzBAdBA
    pass


def mypatch(dest):
    """Do miscellanious patches."""
    guipath = os.path.join(dest, 'data', 'templates', 'guigroup.txt')
    if not os.path.exists(guipath):
        logging.warning("Couldn't find {}".format(guipath))
    # with open(guipath, "w+") as f:
    #     in_text = f.read()
    #     out_text = mod_guigroup(in_text)
    #     f.write(out_text)
    # temporary revert to debug
    with open(guipath, "r") as f:
        in_text = f.read()
    out_text = mod_guigroup(in_text)
    with open(guipath, "w+") as f:
        f.write(out_text)
    replace_haunted1_with_golf1ex1 = True
    if replace_haunted1_with_golf1ex1:
        logging.warning("bye haunted")
        if DO_GOLF1:
            mappath = os.path.join(dest, 'data', 'maps')
            hauntpath = os.path.join(mappath, 'w1_t1.txt')
            ex1path = os.path.join(mappath, 'level01_wp.txt')
            with open(hauntpath, "w+") as f, open(ex1path, 'r') as g:
                new_text = g.read()
                f.write(new_text)
    mute_menu_music = True
    if mute_menu_music:
        logging.info("Muting menu music")
        # editormusic is the main menu - did it switch with menumusic
        # some time in development?
        soundpath = os.path.join(dest, 'data', 'sound')
        edpath = os.path.join(soundpath, 'editormusic.ogg')
        menupath = os.path.join(soundpath, 'menumusic.ogg')
        for mypath in [edpath, menupath]:
            with open(mypath, "w+") as f:
                f.write('')


def list_files(dir):
    """Recursively list all dirs and files in dir."""
    outdirs, outfiles = [], []
    if not os.path.exists(dir):
        logging.critical("Shouldn't happen: does not exist: {}".format(dir))
    for root, dirs, files in os.walk(dir):
        for name in files:
            outfiles.append(os.path.join(root, name))
        for name in dirs:
            outdirs.append(os.path.join(root, name))
    return outdirs, outfiles


def merge_conflicting(file1, file2, full_dest, foot="end"):
    """Merge files which have already been found to be "conflicting".
    i.e., they have the same filename and parent directory structure.
    `full_dest`: the output path including the folder and file name.

    Text files should be concatenated:
    {file1, file2, foot}. If a `foot` is found between
    file1's copy and file2's copy, that `foot` is deleted.
    When a `foot` is not found in a merged file, they are merged anyway.
    Conflicting binary (non-text) files are taken solely from file2
    and logged with a WARNING, because they cannot be merged.

    foot: footer for some files. "end\n" or "end\r\n" for golf1 / golf2.
    Depends on the file. or maybe always \r\n.
    """

    # shallow=True found all identical files. no need for False
    if filecmp.cmp(file1, file2) or is_binary(file2):
        result = 'same_or_binary'
        copy_file(file2, full_dest)
    else:
        result = 'different'
        # filename = os.path.basename(file2)
        ext = os.path.splitext(file2)[1]
        lp = file1
        rp = file2
        with open(lp, "rb") as lf, open(rp, "rb") as rf:
            left = lf.read()
            right = rf.read()
            empty_file = b'end\r\n'
            if (left == empty_file):
                # if right is empty_file too, we still get a file in full_dest
                out = right
                # game hates it when files start with \r\n
                logging.debug('empty: {}'.format(full_dest))
                result = 'empty'
            elif (right == empty_file):
                # if left is empty_file too, we still get a file in full_dest
                out = left
                # game hates it when files start with \r\n
                logging.warning('empty: {}'.format(full_dest))
                result = 'empty'
            elif (len(left) < 10) or (len(right) < 10):
                pdb.set_trace()
                # left is empty file from ex. the fairy DLC
                # and right is already in place at full_dest
                # game hates it when files start with \r\n
                logging.warning('empty: {} {}'.format(left, full_dest))
                result = 'empty'
                return result
            else:
                pass
            # todo probably some bullshit here
            # may not even need unicodedammit if i'm smart about it
            dl = UnicodeDammit(left)
            dr = UnicodeDammit(right)
            if dl.original_encoding != dr.original_encoding:
                logging.warning(
                    "Encodings don't match, taking right of {}{}.format(dl.original_encoding, dr.original_encoding)")

            left = dl.unicode_markup
            right = dr.unicode_markup
            out_enc = dr.original_encoding
            if ext in '.xml':
                # concat XML of a certain format
                foot = '</textbank>'
                header_ind = right.find('<textbank>')  # no slash
                if header_ind == -1:
                    logging.warning("no header in XML!")
                right = right[header_ind:]
            # only get the last occurance of `foot`.
            ind = left.rfind(foot)
            ind_remaining = ind + len(foot)
            remaining = left[ind_remaining:]
            test_regex = '[\n\r]+'
            subbed_remain = re.sub(test_regex, '', remaining)
            if len(subbed_remain) == 0:
                # only test_regex was between `foot` and the end of file (EOF)
                # used to keep `remaining` in the output since it's just whitespace
                # but stopped, in bugfix effort
                # out = left[0:ind] + remaining + right
                out = left[0:ind] + right
            else:
                out = left[0:ind] + right
                r_ind = right.rfind(foot)
                if (r_ind == -1) or ('precache' in lp) or ('dxshaders' in lp):
                    # plain text programming header files?
                    # disturbance_def.fxh seems like bigger files are better / have more cases
                    msg = "{}: No footer, merging as-is".format(rp)
                    if ('precache' in lp) or ('dxshaders' in lp):
                        # logging.warning( "Skipping precache, no idea if it helps")
                        result = 'precache'
                        # return result
                        logging.info('Expected: ' + msg)
                    elif r'art\fonts' in lp:
                        # similar, nearly identical font file. longer file
                        # seems to contain extra characters (ex. Chinese)
                        # but i didn't test thoroughly.
                        logging.info("Taking bigger font")
                        if len(left) > len(right):
                            out = left
                        else:
                            out = right
                    else:
                        logging.warning(msg)
                        logging.debug("{} left side is:\n".format(lp) +
                                      left[-50:] + ".......")
                        logging.debug(
                            "{} right side is:\n".format(rp) + right[-50:])
                        pdb.set_trace()
                    # todo make menu (and other files?) not double up CR LF into two newlines
                    out = left + '\n' + right
                elif (not left.endswith(foot)):
                    logging.debug(lp)
                    logging.debug(rp)
                    num_removed = len(left) - ind
                    message = "removing {} after the footer!".format(
                        num_removed)
                    end_ind = (ind + 30)
                    bad_str = left[ind:end_ind]
                    message = message + '\n' + bad_str + \
                        bytes(bad_str, encoding=out_enc).hex()
                    if num_removed > 100:
                        logging.warning("removing a ton; shouldn't happen")
                    elif num_removed > 4:
                        logging.warning(message)
                    else:
                        logging.info(message)
        # known errors on Linux with Windows paths, don't care
        # newline is critical on windows, otherwise you get '\r\r\n' (2 `\r` s)
        with open(full_dest, "w+", encoding=out_enc, newline='') as newfile:
            newfile.write(out)
    return result


def merge_list(list1, list2):
    """Return `uniques` and `dupes` from two lists."""
    uniques = []
    dupes = []
    # https://stackoverflow.com/questions/9835762/how-do-i-find-the-duplicates-in-a-list-and-create-another-list-with-them
    combined = list1 + list2
    item_counts = [(x, count) for x, g in itertools.groupby(
        sorted(combined)) if (count := len(list(g))) > 0]
    for element in item_counts:
        myitem = element[0]
        mycount = element[1]
        if mycount > 1:
            dupes.append(myitem)
        else:
            uniques.append(myitem)
    return uniques, dupes


def get_path_components(mypath):
    mypath = os.path.normpath(mypath)
    return mypath.split(os.sep)


# https://stackoverflow.com/questions/16891340/remove-a-prefix-from-a-string
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


def merge_dirs(dest_dir, source1, source2):
    """refactored (still in testing) `merge_dirs`.
    Recursively merge source into dest_dir.
    All files are copied to dest_dir.

    Conflicting files (which share a relative path and file name) are passed to
    `merge_conflicting()`.

    Unique files (those with paths that are unique) are copied normally.
    """

    def get_subpath(mypath, split_text):
        potential_subpath = mypath.split(split_text, maxsplit=1)
        try:
            if len(potential_subpath) > 1:
                subpath = potential_subpath[1:]
                subpath = subpath[0]
                # prevent os.path.join from messing up on an 'absolute path'
                subpath = remove_prefix(subpath, "\\")
            elif len(potential_subpath) == 1:
                # most likely, split_text was not found.
                # ex. MiniGolf2.exe in the root folder.
                logging.debug(
                    "No subpath found: {} ||| {}".format(mypath, split_text))
                subpath = ''
            else:
                logging.warning("<= 0 subpath?")
                pdb.set_trace()
        except Exception as e:
            pass
            print("{} ||| {}".format(mypath, split_text))
            print(e)
            pdb.set_trace()

        return subpath

    # input normalization
    dest_dir = dest_dir.rstrip("\\")
    source1 = os.path.join(source1)
    source2 = os.path.join(source2)
    s1_dirs, s1_files = list_files(source1)
    s2_dirs, s2_files = list_files(source2)
    s1_stubs = [get_subpath(mydir, source1) for mydir in s1_dirs]
    s2_stubs = [get_subpath(mydir, source2) for mydir in s2_dirs]
    dirs_to_make = s1_stubs + s2_stubs
    for mydir in dirs_to_make:
        newdir = os.path.join(dest_dir, mydir)
        os.makedirs(newdir, exist_ok=True)
    s1_file_stubs = [get_subpath(myfile, source1) for myfile in s1_files]
    s2_file_stubs = [get_subpath(myfile, source2) for myfile in s2_files]
    uniques, conflicts = merge_list(s1_file_stubs, s2_file_stubs)
    logging.debug("{} conflicts".format(len(conflicts)))
    results = []
    for conflict in (conflicts):
        file1 = os.path.join(source1, conflict)
        file2 = os.path.join(source2, conflict)
        full_dest = os.path.join(dest_dir, conflict)
        if ("Golf1" in file1) or ("Golf1" in file2):
            pass
        results.append(merge_conflicting(file1, file2, full_dest))
    logging.debug("{} uniques".format(len(uniques)))
    for unique in (uniques):
        path1 = os.path.join(source1, unique)
        path2 = os.path.join(source2, unique)
        if os.path.exists(path1):
            src_path = path1
        elif os.path.exists(path2):
            src_path = path2
        else:
            logging.ERROR("No path")
        full_dest = os.path.join(dest_dir, unique)
        if os.path.isfile(full_dest):
            # `(dest_dir not in src_path)` means we're copying to and from the same place
            if (dest_dir not in src_path):
                logging.debug('{} {}'.format(src_path, full_dest))
                # should have been put in `conflicts` list above, not `uniques`.
                logging.warning(
                    "Shouldn't happen: Exists: {}".format(full_dest))
                pdb.set_trace()
            continue
        else:
            copy_file(src_path, full_dest)
    return


if __name__ == "__main__":
    try:
        if True:
            logging.info(
                "Here we go; golf1 and golf2 take about 60 seconds each.")
            os.makedirs(PATH_DEST, exist_ok=True)
            add_content(golf1=PATH_GOLF1, golf1_ex=PATH_GOLF1_EX,
                        golf2=PATH_GOLF2, golf2_ex=PATH_GOLF2_EX,
                        x360=PATH_360, dest_dir=PATH_DEST)
            logging.info('files are in place; patching now')
            mypatch(PATH_DEST)
            controller_warn = "you MUST have a compatible controller (360/XONE) plugged in or the game will crash in weird spots (ex. starting tournemant)"
            logging.warning(controller_warn)
            warning_path = os.path.join(PATH_DEST, "plug_in_controller.txt")
            with open(warning_path, "w+") as f:
                f.write(controller_warn)
        else:
            main_path = os.path.join(
                r"E:\golf\builds\2020-07-26-02_18_25-works - copy-golf1-no-conflicts\data")
            templ_path = os.path.join(main_path, 'templates')
            merge_dirs(templ_path, os.path.join(
                main_path, 'templates_pc'),  templ_path)
            merge_dirs(templ_path, os.path.join(
                main_path, 'templates_contentex01'), templ_path)
    except Exception as e:
        logging.error(e)
